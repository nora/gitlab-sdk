<?php

namespace NORA\GitlabSdk\Usecase;

use League\OAuth2\Client\Token\AccessTokenInterface;
use NORA\GitlabSdk\Enum\Scope;
use NORA\GitlabSdk\GitlabSdk;
use NORA\GitlabSdk\VO\AccessToken;
use NORA\GitlabSdk\VO\AuthorizationUrl;
use NORA\Oauth\AccessTokenInterface as OauthAccessTokenInterface;
use Omines\OAuth2\Client\Provider\Gitlab;

final class CreateOAuthToken
{
    public function __construct(
        private GitlabSdk $sdk
    ) {
    }

    private function getClient(?string $redirect_uri = null): Gitlab
    {
        return new Gitlab(options: $this->sdk->getOAuthClientOption($redirect_uri)->toArray());
    }

    /**
     * @param array<\NORA\GitlabSdk\Enum\Scope> $scope
     */
    public function createAuthUrl(string $redirect_uri, array $scope = [Scope::API,Scope::OPENID]): AuthorizationUrl
    {
        $client = $this->getClient($redirect_uri);
        return new AuthorizationUrl(
            url: $client->getAuthorizationUrl(
                options: [
                    "scope" => array_map(fn ($v) => $v->value, $scope)
                ]
            ),
            state: $client->getState(),
        );
    }

    public function byCode(
        string $redirect_uri,
        string $code
    ): OauthAccessTokenInterface
    {
        $accessToken = $this->getClient($redirect_uri)->getAccessToken('authorization_code', [
            'code' => $code
        ]);
        assert($accessToken instanceof AccessTokenInterface);

        return new AccessToken($accessToken);
    }

    public function __invoke(int $port = 8999): OauthAccessTokenInterface
    {
        $redirect_uri = "http://localhost:{$port}";
        $authUrl = $this->createAuthUrl($redirect_uri);
        echo $authUrl;

        $result = $this->runServer($port);
        if (!$authUrl->checkState($result['state'])) {
            throw new \RuntimeException("State Failed");
        }
        $token = $this->byCode($redirect_uri, $result['code']);
        return $token;
    }

    /**
     * @return array{code: string, state: string}
     */
    public function runServer(int $port = 8999): array
    {
        $server = stream_socket_server(
            "tcp://0.0.0.0:{$port}",
            $errno,
            $errmsg,
            STREAM_SERVER_LISTEN | STREAM_SERVER_BIND
        );

        assert(is_resource($server));

        $query = [];
        while ($conn = stream_socket_accept($server)) {
            echo "\r\nSocket accepted\r\n";
            while ($line = fgets($conn, 1024)) {
                if ($line === "\r\n") {
                    break;
                }
                if (preg_match('/GET (.+) HTTP/', $line, $m)) {
                    $parts = parse_url($m[1]);
                    if (!isset($parts['query'])) {
                        continue;
                    }
                    parse_str($parts['query'], $query);
                    break 2;
                }
            }
            fwrite($conn, "HTTP/1.1 201 OK\r\n");
            fwrite($conn, "\r\n");
            fclose($conn);
        }
        fclose($server);

        assert(isset($query["code"]));
        assert(isset($query["state"]));
        assert(is_string($query["code"]));
        assert(is_string($query["state"]));

        return [
            'code' => $query['code'],
            'state' => $query['state']
        ];
    }
}
