<?php

declare(strict_types=1);

namespace NORA\GitlabSdk\Enum;

enum Scope: string
{
    case API = "api";
    case OPENID = "openid";
    case READ_USER = "read_user";
}
