<?php

declare(strict_types=1);

namespace NORA\GitlabSdk;

use Gitlab\Client;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use NORA\GitlabSdk\VO\AccessToken as VOAccessToken;
use NORA\GitlabSdk\VO\GitlabOAuthClientOption;
use NORA\GitlabSdk\VO\GitlabSdkConfig;
use NORA\Oauth\AccessTokenInterface as OauthAccessTokenInterface;
use NORA\Oauth\AccessTokenRefreshInterface;
use Omines\OAuth2\Client\Provider\Gitlab;


final class GitlabSdk implements AccessTokenRefreshInterface
{
    public function __construct(private GitlabSdkConfig $config)
    {
    }

    public function getClient(): Client
    {
        $client = new Client();
        $client->setUrl("https://{$this->config->getDomain()}");
        return $client;
    }

    public function oauthed(OauthAccessTokenInterface $token): Client
    {
        $client = $this->getClient();
        assert($token instanceof VOAccessToken);
        $client->authenticate($token->getToken(), Client::AUTH_OAUTH_TOKEN);
        return $client;
    }

    public function getOAuthClientOption(?string $redirect_uri = null): GitlabOAuthClientOption
    {
        return new GitlabOAuthClientOption(
            application_id: $this->config->application_id,
            application_secret: $this->config->application_secret,
            redirect_uri: (string) $redirect_uri,
            domain: $this->config->getDomain()
        );
    }

    public function getResourceOwner(OauthAccessTokenInterface $token): ResourceOwnerInterface
    {
        assert($token instanceof VOAccessToken);
        return (new Gitlab(options: $this->getOAuthClientOption()->toArray()))->getResourceOwner($token->getLeagueAccessToken());
    }

    public function refresh(
        OauthAccessTokenInterface $token,
        ?bool &$refreshed = null
    ): OauthAccessTokenInterface {
        if (!$this->isAccessTokenExpired($token)) {
            $refreshed = false;
            return  $token;
        }

        $refreshed = true;
        $gitlab = new Gitlab(options: $this->getOAuthClientOption()->toArray());
        assert($token instanceof VOAccessToken);
        return $gitlab->getAccessToken('refresh_token', [
            'refresh_token' => $token->getLeagueAccessToken()->getRefreshToken()
        ]);
    }

    public function isAccessTokenExpired(OauthAccessTokenInterface $token): bool
    {
        assert($token instanceof VOAccessToken);
        return $token->getLeagueAccessToken()->hasExpired();
    }
}
