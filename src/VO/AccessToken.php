<?php

namespace NORA\GitlabSdk\VO;

use League\OAuth2\Client\Token\AccessToken as TokenAccessToken;
use NORA\Oauth\AccessTokenInterface;

final class AccessToken implements AccessTokenInterface
{
    public function __construct(private TokenAccessToken $token)
    {
    }

    public function getToken() : string
    {
        return $this->token->getToken();
    }

    public static function fromFile(string $path): AccessTokenInterface
    {
        $text = file_get_contents($path);
        return self::fromString($text);
    }

    public static function fromString(string $text): AccessTokenInterface
    {
        return unserialize($text);
    }

    public function __toString(): string
    {
        return serialize($this);
    }

    public function getLeagueAccessToken() : TokenAccessToken
    {
        return $this->token;
    }
}
