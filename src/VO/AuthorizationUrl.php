<?php

namespace NORA\GitlabSdk\VO;

final class AuthorizationUrl
{
    public function __construct(private string $url, private string $state)
    {
    }

    public function __toString(): string
    {
        return $this->url;
    }

    public function checkState(string $state): bool
    {
        return $this->state === $state;
    }

}
