<?php

namespace NORA\GitlabSdk\VO;

final class GitlabOAuthClientOption
{
    public function __construct(
        private string $application_id,
        private string $application_secret,
        private string $redirect_uri,
        private string $domain,
    ) {
    }

    /**
     * @return array{clientId: string, clientSecret: string, domain: string, redirectUri: string}
     */
    public function toArray(): array
    {
        return [
            'clientId' => $this->application_id,
            'clientSecret' => $this->application_secret,
            'domain' => "https://" . $this->domain,
            'redirectUri'  => $this->redirect_uri
        ];
    }
}
