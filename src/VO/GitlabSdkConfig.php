<?php

namespace NORA\GitlabSdk\VO;

final class GitlabSdkConfig
{
    public function __construct(
        public string $application_id = "",
        public string $application_secret = "",
        private ?string $domain = null,
    ) {
    }

    public function getDomain(): string
    {
        if (!$this->domain) {
            return 'gitlab.com';
        }
        assert(is_string($this->domain));
        return $this->domain;
    }
}
