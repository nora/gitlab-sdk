<?php

declare(strict_types=1);

namespace NORA\GitlabSdk;

use NORA\GitlabSdk\VO\GitlabSdkConfig;
use Ray\Di\Di\Named;
use Ray\Di\ProviderInterface;
use Ray\Di\SetContextInterface;

/**
 * @template-implements ProviderInterface<GitlabSdk>
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class GitlabSdkProvider implements ProviderInterface, SetContextInterface
{
    private string $context = 'default';

    public function __construct(
        /** @var array<GitlabSdkConfig> */
        #[Named('gitlab_sdk_configs')] private array $clientConfig,
    ) {
    }

    public function setContext($context): void
    {
        $this->context = $context;
    }

    public function get(): GitlabSdk
    {
        $context = $this->context;

        if (!isset($this->clientConfig[$context])) {
            $context = 'default';
            if (!isset($this->clientConfig[$context])) {
                throw new \RuntimeException('Configuration not found');
            } else {
            }
        }

        assert($this->clientConfig[$context] instanceof GitlabSdkConfig);
        return new GitlabSdk($this->clientConfig[$context]);
    }
}
