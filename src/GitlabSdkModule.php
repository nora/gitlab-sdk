<?php

namespace NORA\GitlabSdk;

use Ray\Di\AbstractModule;

/**
 * @psalm-suppress UnusedClass
 */
class GitlabSdkModule extends AbstractModule
{
    public function __construct(
        /** @var array<\NORA\GitlabSdk\VO\GitlabSdkConfig> */
        private array $configs
    )
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this->bind()->annotatedWith('gitlab_sdk_configs')->toInstance($this->configs);
        foreach(array_keys($this->configs) as $k) {
            if ($k == "default") {
                $this->bind(GitlabSdk::class)->toProvider(GitlabSdkProvider::class, $k);
            }
            $this->bind(GitlabSdk::class)->annotatedWith($k)->toProvider(GitlabSdkProvider::class, $k);
        }
    }
}
