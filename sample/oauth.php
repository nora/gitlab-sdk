<?php

namespace NORA\GitlabSdk;

use NORA\GitlabSdk\Infra\AccessTokenRepo;
use NORA\GitlabSdk\Usecase\CreateOAuthToken;
use NORA\GitlabSdk\VO\AccessToken;
use NORA\GitlabSdk\VO\GitlabSdkConfig;
use NORA\Oauth\Infra\AccessTokenRepo as InfraAccessTokenRepo;
use NORA\Storage\Filesystem\FilesystemStorageOption;
use NORA\Storage\Kvs\KvsFilesystemStorage;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$sdk = new GitlabSdk(new GitlabSdkConfig(
    application_id: getenv('APPLICATION_ID'),
    application_secret: getenv('APPLICATION_SECRET'),
    domain: 'gitlab.avaper.day'
));

$token = (new CreateOAuthToken($sdk))();
$token = $sdk->refresh($token, $changed);

printf("Changed: %s \n", $changed ? 'changed' : 'not changed');
$client = $sdk->oauthed($token);

$tokenRepo = new InfraAccessTokenRepo(
    new KvsFilesystemStorage(
        new FilesystemStorageOption(path: realpath(__DIR__ . "/../tests") . "/var/gitlab-sdk")
    )
);

$tokenRepo->save($token);

// Projectに紐づくIssueを取得
var_dump($client->issues()->all(
    project_id: 274
));

assert($token instanceof AccessToken);

$user = $sdk->getResourceOwner($token);
var_dump($user->getUsername());
