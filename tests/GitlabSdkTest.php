<?php

declare(strict_types=1);

namespace NORA\GitlabSdk;

use NORA\GitlabSdk\Fake\TestModule;
use PHPUnit\Framework\TestCase;
use Ray\Di\Injector;

class GitlabSdkTest extends TestCase
{
    protected Injector $injector;

    protected function setUp(): void
    {
        $this->injector = new Injector(new TestModule());
    }

    public function testIsInstanceOfGitlabSdk(): void
    {
        $sdk = $this->injector->getInstance(GitlabSdk::class);
        $this->assertInstanceOf(GitlabSdk::class, $sdk);
    }
}
