<?php

namespace NORA\GitlabSdk\Fake;

use NORA\GitlabSdk\GitlabSdkModule;
use NORA\GitlabSdk\VO\GitlabSdkConfig;
use Ray\Di\AbstractModule;

final class TestModule extends AbstractModule
{
    public function configure()
    {
        $this->install(new GitlabSdkModule([
            'default' => new GitlabSdkConfig(
                application_id: '3a1b41e19857a768bfab3e2218996a424146a22abce58f2d9913ce96531cc451',
                application_secret: 'gloas-655bef60270484f40a39feb3213a5246e9e97fd33571c5b2ef3213d18109e2a2',
                domain: 'gitlab.avaper.day'
            )
        ]));
    }
}
